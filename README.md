Patient Data Manager Role
=========================

This is a role for ansible-galaxy for URL based roles

Requirements
------------


Role Variables
--------------

See the HSPC Insaller for a list of properties

Dependencies
------------


Example Playbook
----------------


License
-------

Apache2

Author Information
------------------

